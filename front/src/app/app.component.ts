import { Component } from '@angular/core';
import { RestService } from './services/rest-service/rest-service.service';
import { AuthService } from './services/auth-service/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Authentication Point';

  get viewLoadBar(): boolean {
    return this.rest.isLoading;
  }

  get isAliveToken() {
    return this.authService.isAliveToken;
  }

  constructor(private rest: RestService, private authService: AuthService, private router: Router) {}

  quit() {
    this.authService.quit();
    this.router.navigateByUrl('/login');
  }
}
