import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth-service/auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthUserGuard implements CanActivate {
  path: import("@angular/router").ActivatedRouteSnapshot[];
  route: import("@angular/router").ActivatedRouteSnapshot;

  constructor(public router: Router, private authService: AuthService) {}

  canActivate(): Observable<boolean> | boolean {
    return this.authService.isAuthorized().pipe(map((isAuth: boolean) => {
      if (!isAuth) {
        this.router.navigate(['login']);
        return false;
      }
      return isAuth;
    }));
  }
}
