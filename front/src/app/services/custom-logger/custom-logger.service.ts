import { Injectable } from '@angular/core';

@Injectable()
export class CustomLoggerService {

  constructor() {}

  log(...data) {
    console.log(...data);
  }

  info(...data) {
    // tslint:disable-next-line:no-console
    console.info(...data);
  }

  warn(...data) {
    console.warn(...data);
  }

  error(...data) {
    console.error(...data);
  }
}
