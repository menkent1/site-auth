import { Injectable } from '@angular/core';
import { CustomLoggerService } from '../custom-logger/custom-logger.service';
import { RestService } from '../rest-service/rest-service.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { JWTService } from '../jwt-service/jwt.service';
import { JWTInfo } from 'src/app/model/model';

/**
 * Сервис предназначенный для авторищации пользователя
 * Сервис получает токены авторизации
 * Сервис сохраняет токены в LocalStorage через JWT сервис
 * Сервис умеет обновлять токен через RefreshToken
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private logger: CustomLoggerService,
    private rest: RestService,
    private jwtService: JWTService,
    ) { }

  /**
   * Проверяет, не пора ли сделать обновление токена, если пора, то сделает это
   */
  public isAuthorized(): Observable<boolean> {
    if (this.jwtService.jwt.isEmpty) {
      return of(false);
    }

    /**
     * 
     * ПРОВЕРЯТЬ ЧЕРЕЗ СЕРВЕР, ИНАЧЕ МОЖНО БУДЕТ ПРОСТО ДВИГАТЬ ВРЕМЯ
     * 
     * НО С ДРУГОЙ СТОРОНЫ ПРОГРУЗКА ПРОСТО МОДУЛЯ НИЧЕГО НЕ ДАСТ БЕЗ ДОСТУПА К ДАННЫМ С СЕРВЕРА
     * 
     */

    return this.jwtService.isAlive && of(true) || this.rest.refreshToken();
  }

  /**
   * непосредственно обновлене токена с использованием refreshToken
   */
  public refreshToken(): Observable<boolean> {
    return this.rest.call('/refresh', {refreshToken: this.jwtService.refreshToken});
  }

  /**
   * Генерирует токен из данных - нужно для получения токена
   */
  public generateToken(data) {
    return this.jwtService.jwtInfoFromData(data);
  }

  /**
   * Разлогинивание
   */
  public quit() {
    this.jwtService.clearJwtToken();
  }

  public get isAliveToken() {
    return this.jwtService.isAlive;
  }
}
