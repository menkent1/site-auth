import { Injectable } from '@angular/core';
import { LocalStorageService } from '../local-storage/local-storage.service';
import { JWTInfo, Mappable } from 'src/app/model/model';
import { CookiesService } from '../cookies-service/cookies.service';
import { forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

/**
 * сервис предназначен исключительно для хранения JWT пользователя
 * Он только может отдавать JWT или сохранять
 */
@Injectable({
  providedIn: 'root'
})
export class JWTService {

  private readonly jwtStorageName: string = 'jwt-info';
  private readonly jwtTokenCookiesName: string = 'jwt-token';
  private readonly jwtRefreshTokenCookiesName: string = 'jwt-refresh';
  private readonly jwtExpiredCookiesName: string = 'jwt-expired';
  private readonly cookiesOptions: any = {
    domain: environment.domain,
  }

  // todo: Сделать что-то вроде декораторов @LocalStorage, они сами повесят сеттер и геттер для хранения данного значения
  private _jwt: JWTInfo = null;

  public get jwt(): JWTInfo {
    return this._jwt;
  }
  public set jwt(value: JWTInfo) {
    this._jwt = value;
    this.storage.set(this.jwtStorageName, value.toJson());
    this.toCookies();
  }

  constructor(private storage: LocalStorageService, private cookie: CookiesService) {
    this.jwt = new JWTInfo(JSON.parse(this.storage.get(this.jwtStorageName)));
    if (this.jwt.isEmpty) {
      this.fromCookies();
    }
  }

  private fromCookies() {
    forkJoin(
      this.cookie.get(this.jwtTokenCookiesName),
      this.cookie.get(this.jwtRefreshTokenCookiesName),
      this.cookie.get(this.jwtExpiredCookiesName)
    ).pipe(
      map((result: any[]) => {
        const [token, refreshToken, expiration] = result;
        return new JWTInfo({token, refreshToken, expiration});
      }),
      tap(_ => {})
    ).subscribe(jwt => this.jwt = jwt);
  }

  private toCookies() {
    let nowDate = new Date(Date.now());
    let date = new Date(nowDate.setFullYear(nowDate.getFullYear() + 1));
    const expiresObj = {expires: date.toUTCString()};
    const coockieOptions = Object.assign(this.cookiesOptions, expiresObj);
    forkJoin(
      this.cookie.set(this.jwtTokenCookiesName, this.jwt.token, coockieOptions),
      this.cookie.set(this.jwtRefreshTokenCookiesName, this.jwt.refreshToken, coockieOptions),
      this.cookie.set(this.jwtExpiredCookiesName, this.jwt.expiration, coockieOptions)
    ).subscribe();
  }

  public get isAlive(): boolean {
    return this.jwt.isAlive;
  }

  public get token(): string {
    return this.jwt.token;
  }

  public set token(value: string) {
    this.jwt.token = value;
  }

  public get refreshToken(): string {
    return this.jwt.refreshToken;
  }

  public jwtInfoFromData(data) {
    const jwt = new JWTInfo(data);
    if (!jwt.isEmpty) {
      this.jwt = jwt;
    }
    return this.jwt.isAlive;
  }

  public clearJwtToken() {
    this.jwt = new JWTInfo();
    this.jwt.expiration = 0;
    this.jwt.token = this.jwt.refreshToken = '';
  }
}
