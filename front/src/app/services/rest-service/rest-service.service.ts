import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CustomLoggerService } from '../custom-logger/custom-logger.service';
import { JWTService } from '../jwt-service/jwt.service';
import { tap, catchError, filter, map, switchMap, retry } from 'rxjs/operators';
import { JWTInfo } from 'src/app/model/model';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class RestService {

  // заголовки для запросов
  private encodedHeaders: any = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
  private jsonHeaders: any = {'Content-Type': 'application/json; charset=UTF-8'};
  private defaultRequestOptions: { [key: string]: string | object | boolean } = {
    method: 'POST',
    // withCredentials: true
  };

  private countOfActiveCalls = 0;

  get isLoading(): boolean {return this.countOfActiveCalls > 0; }

  constructor(private http: HttpClient, private logger: CustomLoggerService, private jwt: JWTService, private router: Router) {
  }

  call(
    url: string, params: { [key: string]: string | object } = {},
    options: { [key: string]: string | object } = {},
    isNotFirstCall?: boolean): Observable<any> {
    // console.warn('call::', url, params, options, isNotFirstCall);
    const finUrl = options.clearPath ? url :  environment.restPath + url;

    const requestOptions = Object.assign(Object.assign({}, this.defaultRequestOptions), options);
    requestOptions.headers = Object.assign({Authorization: `Bearer ${this.jwt.token}`}, this.jsonHeaders);
    const method = requestOptions.method;

    let request = null;

    if (method === 'POST' || method === 'post') {
      request = this.http.post(finUrl, params, requestOptions);
    } else {
      requestOptions.params = params;
      request = this.http.get(finUrl, requestOptions);
    }

    return request.pipe(
      // tap(_ => this.logger.info('RestService::[' + method + ']', finUrl, '::', _)),
      tap(_ => this.countOfActiveCalls += 1),
      catchError((err) => this.handleError(err, !isNotFirstCall).pipe(switchMap(rr => {
        if (rr && rr.error || isNotFirstCall) {
          return of(rr);
        } else {
          return this.call(url, params, options, true);
        }
      }))),
      // tap(_ => this.logger.info('RestService::[' + method + ']::2::', finUrl, '::', _)),
      tap(_ => this.countOfActiveCalls -= 1),
    );
  }

  private defaultErrorHandler(error) {
    return {error: error.error || true};
  }

  private handleError(error: HttpErrorResponse, needRefreshToken: boolean): Observable<any> {
    console.warn('HTTP ERROR::', error);
    if (needRefreshToken && error && error.error && error.error.statusCode === 401) {
      return this.refreshToken().pipe(
        switchMap((res: boolean) => {
          if (res) {
            return of(res);
          }
          // console.log('handleError::refreshtoken::returnToLoginPage');
          this.router.navigate(['login']);
          return of(this.defaultErrorHandler(error));
        }));
    }
    return of(this.defaultErrorHandler(error));
  }

  public refreshToken() {
    return this.call('/refresh', {refreshToken: this.jwt.refreshToken}).pipe(
      map(( jwt: any ) => {
        // this.logger.log('refreshToken:: refreshToken::', jwt);
        if (!jwt) {
          return false;
        }
        this.jwt.jwt = new JWTInfo(jwt);
        return this.jwt.isAlive;
      }));
  }
}
