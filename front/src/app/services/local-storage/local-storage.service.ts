import { Injectable } from '@angular/core';
import { CustomLoggerService } from '../custom-logger/custom-logger.service';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(private logger: CustomLoggerService = console) {}

  set(key: string, value: any) {
    localStorage.setItem(key, value);
    // this.logger.log('LocalStorageService:: save::', key, value);
  }

  get(key: string) {
    // this.logger.log('LocalStorageService:: get::', key);
    return localStorage.getItem(key);
  }
}
