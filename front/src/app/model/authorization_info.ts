import { Mappable } from "./mappable";


export class AuthorizationInfo extends Mappable {
    private _id: string;
    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }
}
