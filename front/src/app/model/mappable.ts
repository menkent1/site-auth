export class Mappable {

  // toJson
  public toJson(): string {
    return JSON.stringify(this.toMap());
  }

  // toMap
  public toMap(obj: any = this): any {
    return this.objToMap(obj);
  }


  objToMap(obj: any): any {
    if (obj === null) {
      return obj;
    }
    const isObject = obj instanceof Object;
    const isArray = Array.isArray(obj);
    const objValues: any = isArray ? [] : {};
    if (isObject) {
      for (let objectToInspect = obj; objectToInspect !== null; objectToInspect = Object.getPrototypeOf(objectToInspect)) {
        const inspectIsMappable = objectToInspect instanceof Mappable;
        const keys = inspectIsMappable ? Object.getOwnPropertyNames(objectToInspect) : Object.keys(objectToInspect);
        keys.map(key => {
          const descriptor = Object.getOwnPropertyDescriptor(objectToInspect, key);
          if (!inspectIsMappable || descriptor && descriptor.get && descriptor.set && key !== '__proto__') {
            const rawValue = obj[key];
            if (rawValue) {
              objValues[key] = this.objToMap(rawValue);
            } else {
              objValues[key] = rawValue;
            }
          }
        });
      }
    } else if (isArray) {
      return obj.map(el => this.objToMap(el));
    } else {
      return obj;
    }
    return objValues;
  }



  constructor(data?: any) {
    if (data) {
      Object.assign(this, data);
    }
  }
}
