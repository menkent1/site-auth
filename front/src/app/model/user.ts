import { AuthorizationInfo } from "./authorization_info";
import { Mappable } from "./mappable";

export class User extends Mappable {
    private _id: string;  
    private _name: string;  
    private _email: string;   
    private _createDate: string;  
    private _lastUpdate: string;  
    private _lastAuth: string;

    // authInfo {  
    //     vk: { id };  
    //     fb: { id };  
    //     gihub: { id };  
    //     instagram: { id };  
    //     google: { id };  
    //     yandex: { id };  
    //     twitter: { id }  
    // }
    private _authInfo: { [s: string]: AuthorizationInfo; } = {};

    public get id(): string {
        return this._id;
    }
    public set id(value: string) {
        this._id = value;
    }

    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }

    public get email(): string {
        return this._email;
    }
    public set email(value: string) {
        this._email = value;
    }

    public get createDate(): string {
        return this._createDate;
    }
    public set createDate(value: string) {
        this._createDate = value;
    }

    public get lastUpdate(): string {
        return this._lastUpdate;
    }
    public set lastUpdate(value: string) {
        this._lastUpdate = value;
    }

    public get lastAuth(): string {
        return this._lastAuth;
    }
    public set lastAuth(value: string) {
        this._lastAuth = value;
    } 

    public get authInfo(): { [s: string]: AuthorizationInfo; } {
        return this._authInfo;
    }
    public set authInfo(value: { [s: string]: AuthorizationInfo; }) {
        this._authInfo = value;
    }

    constructor(data?: any) {
        super(data);
        if (data) {
            Object.assign(this, data);
        }
    }

    public isConnectedSocial(socialSysName: string) {
        return !!this.authInfo[socialSysName];
    }
}