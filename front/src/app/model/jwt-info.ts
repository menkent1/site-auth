import { Mappable } from "./mappable";

export class JWTInfo extends Mappable {
    private _token: string = '';
    private _refreshToken: string = '';
    private _expiration: number = 0;  // дата истечения - значит пора попробовать использовать рефреш-токен для обновленя
    
    public get token(): string {
        return this._token;
    }
    public set token(value: string) {
        this._token = value;
    }
    
    public get refreshToken(): string {
        return this._refreshToken;
    }
    public set refreshToken(value: string) {
        this._refreshToken = value;
    }
    
    public get expiration(): number {
        return this._expiration;
    }
    public set expiration(value: number) {
        this._expiration = value;
    }


    public get isEmpty(): boolean {
        return !(this.token && this.refreshToken && this.expiration);
    }

    public get isAlive(): boolean {
        // console.warn(this.isEmpty, this.expiration, new Date().getTime(), this.expiration > (new Date().getTime()));
        return !this.isEmpty && this.expiration > (new Date().getTime()); 
    }

    constructor(data?: any) {
        super(data);
        if (data) {
            Object.assign(this, data);
        }
    }
}
