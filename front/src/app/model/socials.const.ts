

export interface Social {
    name: string;
    sysname: string;
    icon?: string;
}

export const Socials: Social[] = [
    {name: 'VK', sysname: 'vk'},
    {name: 'Facebook_DEV', sysname: 'fb'},
    {name: 'Google', sysname: 'google'},
    // {name: 'Instagram', sysname: 'instagram'},
    {name: 'Github', sysname: 'github'},
];