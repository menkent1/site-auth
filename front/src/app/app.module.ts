import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { CustomLoggerService } from './services/custom-logger/custom-logger.service';
import { MatButtonModule, MatIconModule, MatToolbarModule, MatProgressBarModule,
  MatMenuModule, MatCardModule, MatFormFieldModule, MatInputModule, MatDividerModule, MatExpansionModule} from '@angular/material';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,

    MatProgressBarModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,

    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatDividerModule,
    MatExpansionModule,
    NoopAnimationsModule
  ],
  providers: [{ provide: CustomLoggerService, useValue: console }],
  bootstrap: [AppComponent]
})
export class AppModule { }
