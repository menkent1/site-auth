import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { CustomLoggerService } from 'src/app/services/custom-logger/custom-logger.service';
import { RestService } from 'src/app/services/rest-service/rest-service.service';
import { UserInfoServiceModule } from './user-info-service.module';
import { User } from 'src/app/model/user';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { takeUntil, debounceTime, map, tap, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: UserInfoServiceModule
})
export class UserInfoService {
  private _user: BehaviorSubject<User> = new BehaviorSubject(null);
  public user$: Observable<User> = this._user.asObservable();
  public get user(): User {
    return this._user.value;
  }
  public set user(value: User) {
    this._user.next(value);
  }


  public userError$: Subject<string> = new Subject();

  constructor(private readonly logger: CustomLoggerService, private rest: RestService) {
    this.loadUser();
  }

  public loadUser() {
    this.rest.call('/user-info').pipe(
      map(res => {
        this.logger.info('user-info result::', res);
        if (res.error) {
          this.user = null;
          this.userError$.next(res.error);
        } else {
          this.user = new User(res);
        }
        return this.user;
      })
    ).subscribe();
  }

}
