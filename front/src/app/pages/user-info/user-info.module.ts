import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserInfoRoutingModule } from './user-info-routing.module';
import { UserInfoComponent } from './user-info/user-info.component';
import { UserInfoServiceModule } from './services/user-info-service/user-info-service.module';
import { MatButtonModule, MatInputModule } from '@angular/material';

@NgModule({
  declarations: [UserInfoComponent],
  imports: [
    CommonModule,
    UserInfoRoutingModule,
    UserInfoServiceModule,

    MatButtonModule,
    MatInputModule,
  ]
})
export class UserInfoModule { }
