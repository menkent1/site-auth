import { Component, OnInit, OnDestroy } from '@angular/core';
import { CustomLoggerService } from 'src/app/services/custom-logger/custom-logger.service';
import { UserInfoService } from '../services/user-info-service/user-info.service';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { Social, Socials } from 'src/app/model/socials.const';
import { RestService } from 'src/app/services/rest-service/rest-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  private onDestroy$: Subject<void> = new Subject();

  socials: Social[] = Socials;

  get user$() {
    return this.userInfo.user$;
  }

  constructor(private logger: CustomLoggerService, private userInfo: UserInfoService, private rest: RestService, private router: Router) { }

  ngOnInit() {
    // this.user$.pipe(takeUntil(this.onDestroy$)).subscribe(console.warn);
    this.userInfo.userError$.pipe(takeUntil(this.onDestroy$), tap(error => {
      console.log('userError::', error);
      alert(error);
      this.router.navigateByUrl('/login');
    })).subscribe();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  reget() {
    this.userInfo.loadUser();
  }

  oauthConnect(socialSysName: string) {
    this.logger.log('connect to::', socialSysName);
    this.rest.call(`/social-redirect/${socialSysName}`).subscribe(res => {
      if (res.link) {
        location = res.link;
      } else {
        this.logger.error(`Redirect to ${socialSysName} not supported!`);
      }
    });
  }

}
