import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, combineLatest, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil, switchMap, tap, map } from 'rxjs/operators';
import { CustomLoggerService } from 'src/app/services/custom-logger/custom-logger.service';
import { RestService } from 'src/app/services/rest-service/rest-service.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {

  private onDestroy$ = new Subject();

  constructor(
    private logger: CustomLoggerService,
    private router: Router,
    private route: ActivatedRoute,
    private rest: RestService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    // получить параметры: провайдера OAuth и значение токена и других полей
    combineLatest(this.route.queryParams, this.route.params, this.authService.isAuthorized()).pipe(
      takeUntil(this.onDestroy$),
      switchMap(([queryParams, {social}, isAuthorized]) => {
        this.logger.info('AuthComponent::from social params::', social, queryParams);
        // Запросить на сервере токены
        const qParams = {...queryParams, ...{isAuthorized: isAuthorized ? '1' : '0' } };
        if (social && queryParams) {
          return this.rest.call(`/auth/${social}`, qParams).pipe(map(res => {
            if (!res.error) {
              return this.authService.generateToken(res);
            } else {
              alert(res.error);
            }
          }));
        }
        return of(null);
      })
    ).subscribe(isAlive => {
      this.logger.info('AuthComponent::JWT is Alive::', isAlive);
      if (!isAlive) {
        // todo: выдать сообщение об ошибке, т.к. не получилось получить токен с сервера
        this.logger.error('Error of get token!');
        // alert('Error of get token!');s
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/']);
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
