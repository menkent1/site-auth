import { Component, OnInit } from '@angular/core';
import { CustomLoggerService } from 'src/app/services/custom-logger/custom-logger.service';
import { RestService } from 'src/app/services/rest-service/rest-service.service';
import { Social, Socials } from 'src/app/model/socials.const';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  socials: Social[] = Socials;

  constructor(private logger: CustomLoggerService, private rest: RestService, private authService: AuthService) { }

  ngOnInit() {
  }

  oauthLogin(social: string) {
    this.logger.log('LoginComponent::oauthLogin::', social);
    this.authService.quit();
    this.rest.call(`/social-redirect/${social}`).subscribe(res => {
      if (res.link) {
        location = res.link;
      } else {
        this.logger.error(`Redirect to ${social} not supported!`);
      }
    });
  }

}
