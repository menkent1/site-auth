export const environment = {
  production: true,
  restPath: 'https://auth-srv.menkent.dev',
  domain: 'menkent.dev',
};
