# site-auth

## TOTO:
- Не забыть про CSRF токен через куку для авторизаций - https://learn.javascript.ru/csrf
- Cookie не работают, потому что сначала нужно удалить старые кукие с токенами
- Сделать localhost авторизацию для того, чтоб удобно работать с локальной машиной
- FINGERPRINT - https://www.npmjs.com/package/express-fingerprint - использовать при создании токена
- Сделать Политику Конфидициальности как здесь (https://vizor-interactive.com/documents/instant-games/privacy-policy.htm)


## auth.menkent.dev  
- Авторизация должна быть сквозная между всеми частями сайта.
- Логин через фб, вк, гугл.
- Использовать JWT для сквозного доступа ко всем частям сайта.

## Основные положения  
- back сделан на node nestjs  (https://nestjs.com)
- back доступен по auth-srv.menkent.dev  
- front нужно сделать на ангуляре 8 (с использованием реактивных форм, если формы будут нужны)
- front пусть так же кладётся куда нужно  
- сквозная авторизация через Bearer


## Организация  
Организация фронта и бека сделана раздельно:  
- фронт лежит в папке front, собирается отдельно и сам кладётся в нужную папку на сервере  
- бек лежит в папке back и ... потом придумаю как его правильно билдить и запускать


## Работа с NestJS  
1. Установка CLI: npm i -g @nestjs/cli  
2. Создане проекта: nest new project-name  
3. Добавление mongoose https://docs.nestjs.com/techniques/mongodb  
4. Добавление авторизации passport.js и passport-jwt.js  https://docs.nestjs.com/techniques/authentication



## Структура Anguar-app фронт-части
1. / - главная страница с отображением: имени, ролей, аудиенций и тд, с возмоностью редактировать
2. /login - страничка с предложением авторизации через соц.сети и тд
3. /auth - страничка-обратный вызов для всех OAuth 


## Структура авторизационного сервера
1. /  - возвращает "auth work", чтоб показать, что авторизационный сервер работает  
todo: позже будет возвращать статистику по кол-ву пользователей, по количеству обращений к серверу и тд
2. /auth - непосредственно попытка авторизации
    - /auth/login
    - /auth/vk 
    - /auth/fb
    - /auth/github
    - /auth/instagram
    - /auth/google
    - /auth/yandex
    - /auth/twitter
3. /check - возвращает расшифрованный JWT с информацией о пользователе и ролях
4. /roles - список ролей пользователя
    - /roles/add - добавление роли к ролям пользователя
    - /roles/del - удаление роли из ролей пользователя
5. /audience - список аудиенций пользователя
    - /audience/add - добавление
    - /audience/del - удаление
6. /user - scope для редактирования информации о юзере
7. /connect - коннект для других соц. сетей, чтоб они просто подключались к пользователю
8. /refresh - обновление JWT по рефреш-токену


## Состав класса User  
```
class User {  
    id,  
    name,  
    email,  
    authInfo {  
        vk: { id },  
        fb: { id },  
        gihub: { id },  
        instagram: { id },  
        google: { id },  
        yandex: { id },  
        twitter: { id }  
    },  
    createDate,  
    lastUpdate,  
    lastAuth,  
}  
```

## JWT Payload
[Основные сведения](https://www.eclipse.org/community/eclipse_newsletter/2017/september/article2.php)  
#### Payload Example  
```
{
  "iss": "https://server.example.com",          // Кто выдал данный токен
  "aud": "s6BhdRkqt3",                          // Для чего он был выдан
  "exp": 1311281970,                            // Время завершения, после которого данный JWT не обрабатывается
  "iat": 1311280970,                            // Когда был сгенерирован данный JWT
  "sub": "24400320",                            // Subject - Предмет (Пользователь), которому был выдан JWT  
  "upn": "jdoe@server.example.com",             // Имя предмета (Пользователя), которому был выдан JWT
  "groups: ["red-group", "admin-group"],        // Группы для Предмета JWT
  // Не обязательные поля
  "roles": ["auditor", "administrator"],        // Роли пользователя
  "jti": "a-123",                               // Уникальный идентификатор выдающего JWT
  [key]: string                                 // И остальные любые поля
}

```