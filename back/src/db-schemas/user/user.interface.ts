import { Document } from 'mongoose';

export interface IAuthorizationInfo {
    [key: string]: string | number,
}

export interface IUser extends Document {
    _id?: string,
    name: string,
    createDate: number,
    lastUpdate?: number,
    lastAuth?: number,
    authInfo: {[key: string]: IAuthorizationInfo},
    roles?: string[],
};