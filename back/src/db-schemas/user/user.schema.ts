import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
    // _id: {type: String, unique: true},
    name: {type: String, required: true},
    createDate: {type: Number},
    lastUpdate: {type: Number},
    lastAuth: {type: Number},
    authInfo: {type: Map},
    roles: {type: [String]}
}, { strict: false });


