import * as mongoose from 'mongoose';

export const SocialParamsSchema = new mongoose.Schema({
    // _id: {type: String, unique: true},
    socialName: {type: String},
    OAUTH_URI: {type: String},
    REDIRECT_URI: {type: String},
    CLIENT_ID: {type: String},
    SECRET_ID: {type: String},
    ACCESS_TOKEN: {type: String},
    GET_USER_INFO_URI: {type: String},
    API_VERSION: {type: String},
    SCOPE: {type: [String]},
}, { strict: false });


