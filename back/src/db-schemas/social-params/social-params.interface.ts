import { Document } from 'mongoose';

export interface ISocialParams extends Document {
    _id?: string,
    socialName: string,
    OAUTH_URI: string,
    REDIRECT_URI: string,
    CLIENT_ID: string,
    SECRET_ID: string,
    ACCESS_TOKEN: string,
    GET_USER_INFO_URI: string,
    API_VERSION?: string,
    SCOPE?: string[],
};