
export default {
    // Application Settings
    port: '8082',
    startMode: 'production',

    // DataBase Settings
    DB_HOST: 'mongodb://mongo:27017/menkentdb',

    // Redirects Links
    loginPage: 'http://auth.menkent.dev',

    JWT_PRIVATE_KEY: './../keys/private_key.pem',
    JWT_PUBLIC_KEY: './../keys/public_key.pem',

};
