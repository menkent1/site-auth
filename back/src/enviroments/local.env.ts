export default {
    // Application Settings
    port: '8082',
    startMode: 'local',

    // DataBase Settings
    DB_HOST: 'mongodb://localhost:27017/menkentdb',

    // Redirects Links
    loginPage: 'http://localhost',

    JWT_SECRET_KEY: 'SecretKeyExample',

};
