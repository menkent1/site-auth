export default {
    // Application Settings
    port: '80',

    // DataBase Settings
    DB_HOST: 'mongodb://localhost:27017/menkentdb',

    // Redirects Links
    loginPage: 'http://localhost',

    // SecretKey - load from DB
    JWT_SECRET_KEY: 'secretKey',
    JWT_PRIVATE_KEY: './keys/private_key.pem',
    JWT_PUBLIC_KEY: './keys/public_key.pem',

};
