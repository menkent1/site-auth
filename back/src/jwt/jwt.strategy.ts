import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JWTPayload } from './jwt-payload.interface';
import { AuthService } from '../services/auth-service/auth-service.service';
import { AuthLogger } from '../services/auth-logger/auth-logger.service';
import { ConfigService } from '../services/config/config.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService, private readonly logger: AuthLogger, private config: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // secretOrKey: config.get('JWT_SECRET_KEY'),
      // publicKey: config.publicJWTKey,
      // privateKey: config.privateJWTKey
      secretOrKey: config.privateJWTKey,
    });
  }

  async validate(payload: JWTPayload) {
    this.logger.info('JwtStrategy::validate', payload);
    const userIsValid: boolean = await this.authService.validateUser(payload);
    if (!userIsValid) {
      throw new UnauthorizedException();
    }
    return userIsValid;
  }
}