import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { JwtService } from '@nestjs/jwt';
import { AuthLogger } from '../services/auth-logger/auth-logger.service';

@Injectable()
export class UserIdMiddleware implements NestMiddleware {
  constructor(private readonly jwtService: JwtService, private readonly logger: AuthLogger) {

  }
  use(req: Request, res: Response, next: Function) {
    const jwtStr = req.headers['authorization'] && req.headers['authorization'].replace('Bearer ', '');
    const payload = jwtStr && this.jwtService.decode(jwtStr);
    this.logger.info('UserIdMiddleware::', payload);
    if (payload) {
      req['additionalParams'] = {user_id: payload.sub};
    }
    next();
  }
}
