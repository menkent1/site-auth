
export interface JWTPayload {
    iss: string,                // "https://server.example.com",            // Кто выдал данный токен
    aud: string,                // "s6BhdRkqt3",                            // Для чего он был выдан
    exp?: number,                // 1311281970,                             // Время завершения, после которого данный JWT не обрабатывается - Выставляется автоматически
    iat: number,                // 1311280970,                              // Когда был сгенерирован данный JWT
    sub: string,                // "24400320",                              // Subject - Предмет (Пользователь), которому был выдан JWT  
    upn: string,                // "jdoe@server.example.com",               // Имя предмета (Пользователя), которому был выдан JWT
    groups?: string[],           // ["red-group", "admin-group"],           // Группы для Предмета JWT
    // Не обязательные поля c точки зрения Стандартов работы JWT
    roles: string[],            // ["auditor", "administrator"],            // Роли пользователя
    jti?: string,                // "a-123",                                // Уникальный идентификатор выдающего JWT
    [key: string]: any,                                                     // И остальные любые поля
}
