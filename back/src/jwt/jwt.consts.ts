export const JWT_ISSUER = 'auth.menkent.dev';
export const JWT_AUDIENCE = '*.menkent.dev';
export const JWT_EXPIRE = 7 * 24 * 3600; // 7d in seconds
export const JWT_REFRESH_EXPIRE = 30 * 24 * 3600; // in seconds
export const JWT_IDENTIFY = '123456112233'; // todo: взять из какого-то файла-ключа
