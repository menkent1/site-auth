export function generateURL(url, params: Array<{ [key: string]: string | number}> = []) {
    params.map( obj => {
        for (let key in obj) {
            url.searchParams.append(key, obj[key]);
        }
    });
}

export function dateAsNumber(date: Date): number {
    return date.getTime();
}

export function numberAsDate(ms: number): Date {
    return new Date(ms);
}

export function getNowDate(): Date {
    return numberAsDate(getNowNumberDate());
}

export function getNowNumberDate(): number {
    return Date.now();
}
