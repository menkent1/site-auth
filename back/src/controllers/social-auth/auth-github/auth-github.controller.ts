import { Controller } from '@nestjs/common';
import { GITHUB_ACCESS_TOKEN, GITHUB_CLIENT_ID, GITHUB_SECRET_ID, GITHUB_GET_USER_INFO } from '../../social.consts';
import { AuthBaseController } from '../auth-base/auth-base.controller';

@Controller('auth/github')
export class AuthGithubController extends AuthBaseController {
    protected userIdFieldName = 'id';
    protected socialSysName = 'github';

    protected getAccessTokenFromData(accessTokenString: string) {
        const accessPath = accessTokenString.split('&');
        const accessToken = accessPath[0].split('=');
        return accessToken[1];
    }

    protected getNameFromData(data: any) {
        return data.login;
    }
}
