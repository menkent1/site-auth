import { Controller, Post, Body, HttpService, Request } from '@nestjs/common';
const querystring = require('querystring');
import { AxiosResponse } from 'axios';
import { UserService } from './../../..//services/user/user.service';
import { IUser } from '../../../db-schemas/user/user.interface';
import { AuthService } from '../../../services/auth-service/auth-service.service';
import { AuthLogger } from '../../../services/auth-logger/auth-logger.service';
import { ConfigService } from '../../../services/config/config.service';
import { map, tap } from 'rxjs/operators';
import { VK_ACCESS_TOKEN, VK_CLIENT_ID, VK_SECRET_ID } from '../../../controllers/social.consts';

@Controller('auth/base')
export class AuthBaseController {
    constructor(
        protected readonly logger: AuthLogger,
        protected readonly userService: UserService,
        protected readonly httpService: HttpService,
        protected readonly authService: AuthService,
        protected readonly config: ConfigService,
    ) {}

    protected header = { 'Content-Type': 'application/x-www-form-urlencoded' };

    protected getHeader() { return this.header; }
    // protected getHeader() { return { 'Content-Type': 'application/x-www-form-urlencoded' }; }

    protected accessTokenData = {};
    protected userInfoData = {};

    // Social params
    protected accessTokenFieldName = 'access_token';
    protected userIdFieldName = 'sub'; // sub or userId
    protected socialSysName = ''; // google

    private _accessTokenUrl = ''; // GOOGLE_ACCESS_TOKEN
    private _redirectUri = ''; // REDIRECT_URI_GOOGLE
    private _clientId = ''; // GOOGLE_CLIENT_ID
    private _secretId = ''; // GOOGLE_SECRET_ID
    private _userInfoUrl = ''; // GOOGLE_GET_USER_INFO

    // Метод для получения имени пользователя. Для каждой Social сделать свой
    protected getNameFromData(data: any) {
        return data.given_name;
    }

    // Создание хеадера для авторизации
    protected generateAuthBearerHeader(token) {
        return { Authorization: `Bearer ${token}` };
    }

    // Получение accessToken
    protected getAccessTokenData(code: string): Promise<AxiosResponse> {
        return this.httpService.post(
            new URL(this.accessTokenUrl).toJSON(),
            querystring.stringify({
                redirect_uri: this.redirectUri,
                code: code,
                client_id: this.clientId,
                client_secret: this.secretId,
                grant_type: 'authorization_code',
            }),
            {headers: this.getHeader()},
        ).toPromise();

    }

    // достаём access_token из пришедших данных
    protected getAccessTokenFromData(accessTokenData) {
        return accessTokenData[this.accessTokenFieldName];
    }

    // Получение UserId (через специальный запрос, а для ВК просто из accessTokenData)
    protected async getSocialUserId(accessToken) {
        const userInfoRequest = await this.httpService.get(
            this.userInfoUrl,
            {headers: this.generateAuthBearerHeader(accessToken) },
        ).toPromise();
        this.userInfoData = userInfoRequest.data;
        return this.userInfoData[this.userIdFieldName];
    }

    // Получение или регистрация пользователя
    protected async getOrRegisterUser(accessToken: string, socialUserId: string, userData: any): Promise<IUser> {
        // Ищем юзера в бд. Если есть, то обновляем токены, иначе регистрируем и выдаём токены
        let user: IUser = await this.userService.findUserByAuth({[this.socialSysName]: {id: socialUserId}});
        if (user) {
            // todo: получили юзера, который уже был в системе, значит можно заменить refresh token в базе или проапгрейдить дату последнего входа
        } else {
            // todo: если пользователя нет, то запросить больше информации и зарегистрировать его
            user = await this.userService.registerUser(this.getNameFromData(userData), { [this.socialSysName]: { id: socialUserId } });
        }
        return user;
    }

    @Post()
    async authUserHandler(@Body() reqBody: {[key: string]: string }, @Request() req: Request): Promise<any> {
        // this.logger.info('AuthGoogleController::authUserHandler::code', reqBody);
        const code = reqBody.code;
        const needConnect = reqBody.isAuthorized === '1';
        const jwtMap = {
            token: null,
            refreshToken: null,
            expiration: 0,
        };

        try {
            const promise11 = this.getAccessTokenData(code);
            const {data: tokenResponse} = await promise11;
            this.accessTokenData = tokenResponse;
            const accessToken: string = this.getAccessTokenFromData(this.accessTokenData);
            const userId = await this.getSocialUserId(accessToken);
            if (!userId || !accessToken) {
                throw 'No found userId or access_token';
            }
            let user: IUser;
            const authorisedUserId = req['additionalParams'] && req['additionalParams'].user_id;
            if (needConnect && authorisedUserId) {
                // Для коннекта нужно
                // поискать, вдруг такой пользователь уже есть с таким socialId
                const userFinded: IUser = await this.userService.findUserByAuth({[this.socialSysName]: {id: userId}});
                if (userFinded) {
                    throw 'User Finded and connect no allowed';
                }
                // получить текущего залогиненого пользователя
                const userToConnectId = authorisedUserId;
                const userToConnect: IUser = await this.userService.findUserById(userToConnectId);
                if (!userToConnect) {
                    throw 'User for connect not found';
                }
                await this.userService.updateAuthInfo(userToConnect, {[this.socialSysName]: {id: userId}});
                user = userToConnect;
            } else {
                // Ищем юзера в бд. Или регистрируем его, если не нашли
                user = await this.getOrRegisterUser(accessToken, userId, this.userInfoData);
            }

            // Выдать пользователю JWT токены (authToken и refreshToken)
            if (user) {
                const jwt = await this.authService.signIn(user);
                const jwtRefresh = await this.authService.signInRefreshToken(user);
                jwtMap.token = jwt;
                jwtMap.refreshToken = jwtRefresh;
                jwtMap.expiration = this.authService.expirationToken();
            }
        } catch (e) {
            this.logger.warn('AuthBaseController::Exception', e && e.response || e);
            return {error: 'Server Error'};
        }

        return jwtMap;
    }


    /**
     * Настройки из БД для всех соц сетей
     */

    protected get accessTokenUrl() {
        if (!this._accessTokenUrl) {
            this.accessTokenUrl = this.config.getSocialParams(this.socialSysName, 'ACCESS_TOKEN');
        }
        return this._accessTokenUrl;
    }
    protected set accessTokenUrl(value) {
        this._accessTokenUrl = value;
    }
    
    protected get redirectUri() {
        if (!this._redirectUri) {
            this.redirectUri = this.config.getSocialParams(this.socialSysName, 'REDIRECT_URI');
        }
        return this._redirectUri;
    }
    protected set redirectUri(value) {
        this._redirectUri = value;
    }
    
    protected get clientId() {
        if (!this._clientId) {
            this.clientId = this.config.getSocialParams(this.socialSysName, 'CLIENT_ID');
        }
        return this._clientId;
    }
    protected set clientId(value) {
        this._clientId = value;
    }
    
    protected get secretId() {
        if (!this._secretId) {
            this.secretId = this.config.getSocialParams(this.socialSysName, 'SECRET_ID');
        }
        return this._secretId;
    }
    protected set secretId(value) {
        this._secretId = value;
    }
    
    protected get userInfoUrl() {
        if (!this._userInfoUrl) {
            this.userInfoUrl = this.config.getSocialParams(this.socialSysName, 'GET_USER_INFO_URI');
        }
        return this._userInfoUrl;
    }
    protected set userInfoUrl(value) {
        this._userInfoUrl = value;
    }
}
