import { Test, TestingModule } from '@nestjs/testing';
import { AuthBaseController } from './auth-base.controller';

describe('AuthBaseController', () => {
  let controller: AuthBaseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthBaseController],
    }).compile();

    controller = module.get<AuthBaseController>(AuthBaseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
