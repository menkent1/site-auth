import { Controller } from '@nestjs/common';
import { AuthBaseController } from '../auth-base/auth-base.controller';

@Controller('auth/fb')
export class AuthFbController extends AuthBaseController {
    protected userIdFieldName = 'id';
    protected socialSysName = 'fb';

    protected getNameFromData(data: any) {
        return data.name;
    }
}
