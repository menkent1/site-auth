import { Test, TestingModule } from '@nestjs/testing';
import { AuthFbController } from './auth-fb.controller';

describe('AuthFb Controller', () => {
  let controller: AuthFbController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthFbController],
    }).compile();

    controller = module.get<AuthFbController>(AuthFbController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
