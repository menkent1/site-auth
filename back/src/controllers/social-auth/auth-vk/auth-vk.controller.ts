import { Controller } from '@nestjs/common';
import { VK_SECRET_ID, VK_ACCESS_TOKEN, VK_CLIENT_ID, VK_GET_USER_INFO, VK_API_VERSION } from '../../social.consts';
import { IUser } from '../../../db-schemas/user/user.interface';
import { generateURL } from '../../../utils/utils';
import { AuthBaseController } from '../auth-base/auth-base.controller';

@Controller('auth/vk')
export class AuthVkController extends AuthBaseController {
    protected userIdFieldName = 'sub';
    protected socialSysName = 'vk';

    protected async getSocialUserId(accessToken) {
        return this.accessTokenData['user_id'] + '';
    }

    protected async getOrRegisterUser(accessToken: string, socialUserId: string): Promise<IUser> {
        // Ищем юзера в бд. Если есть, то обновляем токены, иначе регистрируем и выдаём токены
        let user: IUser = await this.userService.findUserByAuth({vk: {id: socialUserId}});
        if (user) {
            // this.logger.info('FINDED USER', user);
        } else {
            // todo: если пользователя нет, то запросить больше информации и зарегистрировать его
            const userInfoUrl = new URL(VK_GET_USER_INFO);
            generateURL(userInfoUrl, [{ access_token: accessToken, v: this.config.getSocialParams(this.socialSysName, 'API_VERSION') }]);
            const userInfoResponse = await this.httpService.get(
                userInfoUrl.toJSON(),
                { headers: this.getHeader() },
            ).toPromise();

            if (userInfoResponse.status === 200) {
                const vkRes = userInfoResponse.data.response[0];
                if (vkRes) {
                    user = await this.userService.registerUser(vkRes.first_name, { vk: { id: vkRes.id + '' } });
                }
            }
        }
        return user;
    }
}
