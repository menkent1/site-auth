import { Test, TestingModule } from '@nestjs/testing';
import { AuthVkController } from './auth-vk.controller';

describe('AuthVk Controller', () => {
  let controller: AuthVkController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthVkController],
    }).compile();

    controller = module.get<AuthVkController>(AuthVkController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
