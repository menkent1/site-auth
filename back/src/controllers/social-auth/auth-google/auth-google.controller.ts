import { Controller } from '@nestjs/common';
import { GOOGLE_ACCESS_TOKEN, GOOGLE_CLIENT_ID, GOOGLE_SECRET_ID, GOOGLE_GET_USER_INFO } from '../../social.consts';
import { AuthBaseController } from '../auth-base/auth-base.controller';

@Controller('auth/google')
export class AuthGoogleController extends AuthBaseController {
    protected userIdFieldName = 'sub';
    protected socialSysName = 'google';
}
