import { Controller, Get, Param, UseGuards, Post, Req } from '@nestjs/common';
import { UserService } from '../../services/user/user.service';
import { IUser } from '../../db-schemas/user/user.interface';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../../services/auth-service/auth-service.service';
import { AuthLogger } from '../../services/auth-logger/auth-logger.service';

@Controller('user-info')
export class UserInfoController {
    constructor(
        private readonly userService: UserService,
        private readonly authService: AuthService,
        private readonly logger: AuthLogger,
    ) {}

    @Get(':id') 
    @UseGuards(AuthGuard())
    async main(@Param('id') id) {
        // this.logger.info('UserInfoController:: ', id);
        const user: IUser = await this.userService.findUserById(id);
        return user;
    }

    @Post()
    @UseGuards(AuthGuard())
    async userInfo(@Req() req) {
        // this.logger.info('UserInfoController:: ', req['additionalParams']);
        const userId = req['additionalParams'].user_id;
        const user: IUser = await this.userService.findUserById(userId);
        return user || {error: 'not found'};
    }
}
