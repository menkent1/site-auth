import { Controller, Get, Res, HttpStatus, Post } from '@nestjs/common';
import { generateURL } from '../../utils/utils';
import { ConfigService } from '../../services/config/config.service';

@Controller('social-redirect')
export class SocialRedirectController {
    constructor(private config: ConfigService) {}

    @Post('vk')
    redirectToVK() {
        const redirectURL = new URL(this.config.getSocialParams('vk', 'OAUTH_URI'));
        generateURL(redirectURL, [
            {
                'client_id': this.config.getSocialParams('vk', 'CLIENT_ID'),
                'redirect_uri': this.config.getSocialParams('vk', 'REDIRECT_URI'),
                'response_type': 'code',
                'display': 'page',
                'v': this.config.getSocialParams('vk', 'API_VERSION'),
            },
        ]);
        return {link: redirectURL.toJSON()}
    }

    @Post('google')
    redirectToGoogle() {
        const redirectURL = new URL(this.config.getSocialParams('google', 'OAUTH_URI'));
        generateURL(redirectURL, [
            {
                'client_id': this.config.getSocialParams('google', 'CLIENT_ID'),
                'redirect_uri': this.config.getSocialParams('google', 'REDIRECT_URI'),
                'response_type': 'code',
                'scope': this.config.getSocialParams('google', 'SCOPE'),
                'access_type': 'online'
            },
        ]);
        return {link: redirectURL.toJSON()}
    }

    @Post('fb')
    redirectToFB() {
        const redirectURL = new URL(this.config.getSocialParams('fb', 'OAUTH_URI'));
        generateURL(redirectURL, [
            {
                'client_id': this.config.getSocialParams('fb', 'CLIENT_ID'),
                'redirect_uri': this.config.getSocialParams('fb', 'REDIRECT_URI'),
                'response_type': 'code',
                'scope': this.config.getSocialParams('fb', 'SCOPE'),
                'access_type': 'online'
            },
        ]);
        return {link: redirectURL.toJSON()}
    }

    @Post('github')
    redirectToGithub() {
        const redirectURL = new URL(this.config.getSocialParams('github', 'OAUTH_URI'));
        generateURL(redirectURL, [
            {
                'client_id': this.config.getSocialParams('github', 'CLIENT_ID'),
                'redirect_uri': this.config.getSocialParams('github', 'REDIRECT_URI'),
                'response_type': 'code',
                'scope': this.config.getSocialParams('github', 'SCOPE'),
                'access_type': 'online'
            },
        ]);
        return {link: redirectURL.toJSON()}
    }
}
