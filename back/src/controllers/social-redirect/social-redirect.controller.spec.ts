import { Test, TestingModule } from '@nestjs/testing';
import { SocialRedirectController } from './social-redirect.controller';

describe('SocialRedirect Controller', () => {
  let controller: SocialRedirectController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SocialRedirectController],
    }).compile();

    controller = module.get<SocialRedirectController>(SocialRedirectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
