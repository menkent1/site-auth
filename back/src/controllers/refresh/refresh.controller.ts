import { Controller, Post, Body, Param } from '@nestjs/common';
import { AuthLogger } from '../../services/auth-logger/auth-logger.service';
import { AuthService } from '../../services/auth-service/auth-service.service';

@Controller('refresh')
export class RefreshController {

    constructor(private readonly logger: AuthLogger, private readonly authService: AuthService) {}

    @Post() 
    async refresh(@Body('refreshToken') refreshToken) {
        this.logger.info('RefreshController:: refresh');
        return {
            token: await this.authService.getTokenByRefreshToken(refreshToken), 
            expiration:  this.authService.expirationToken(),
            refreshToken: refreshToken
        };
    }
}
