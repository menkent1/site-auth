import { Controller, Body, Post, Get, Param } from '@nestjs/common';
import { AuthLogger } from '../../services/auth-logger/auth-logger.service';
import { AuthService } from '../../services/auth-service/auth-service.service';

@Controller('check')
export class CheckController {

    constructor(private readonly logger: AuthLogger, private readonly authService: AuthService) {}
    

    @Post() 
    async postGetRoles(@Body('token') token) {
        this.logger.info('CheckController:: postGetRoles');
        return { roles: await this.getRoles(token) };
    }

    @Get()
    async getGetRoles(@Param('token') token) {
        this.logger.info('CheckController:: getGetRoles');
        return { roles: await this.getRoles(token) };
    }

    private async getRoles(token: string) {
        return await this.authService.getTokenRoles(token);
    }
}
