import { Module, HttpModule, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from './app.service';
import { SocialRedirectController } from './controllers/social-redirect/social-redirect.controller';
import { AuthVkController } from './controllers/social-auth/auth-vk/auth-vk.controller';
import { UserSchema } from './db-schemas/user/user.schema';
import { UserService } from './services/user/user.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UserInfoController } from './controllers/user-info/user-info.controller';
import { JwtStrategy } from './jwt/jwt.strategy';
import { AuthService } from './services/auth-service/auth-service.service';
import { AuthLogger } from './services/auth-logger/auth-logger.service';
import { USER_SCHEMA, SOCIAL_PARAMS_SCHEMA } from './db-schemas/db-schema.consts';
import { ConfigModule } from './services/config/config.module';
import { RefreshController } from './controllers/refresh/refresh.controller';
import { CheckController } from './controllers/check/check.controller';
import { UserIdMiddleware } from './jwt/jwt.middleware';
import { AuthGoogleController } from './controllers/social-auth/auth-google/auth-google.controller';
import { AuthFbController } from './controllers/social-auth/auth-fb/auth-fb.controller';
import { AuthGithubController } from './controllers/social-auth/auth-github/auth-github.controller';
import { AuthBaseController } from './controllers/social-auth/auth-base/auth-base.controller';
import { ConfigService } from './services/config/config.service';
import { SocialParamsSchema } from './db-schemas/social-params/social-params.schema';
import { DBConfigService } from './services/db-config/db-config.service';

@Module({
  imports: [
    ConfigModule,
    HttpModule,

    /* DATABASE STETTINGS */
    // connect to mongo from docker-compose - требуется единая docker network
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        uri: config.get('DB_HOST'),
        useNewUrlParser: true,
      }),
      inject: [ConfigService],
    }),

    MongooseModule.forFeature([
      { name: USER_SCHEMA, schema: UserSchema, collection: 'users' },
      { name: SOCIAL_PARAMS_SCHEMA, schema: SocialParamsSchema, collection: 'SocialParams' },
    ]),

    /* JWT STETTINGS */
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        // посмотреть здесь аргументы и, вероятно, использовать какие-то другие ключи
        // secret: config.get('JWT_SECRET_KEY'), // 'secretKey'
        publicKey: config.publicJWTKey,
        privateKey: config.privateJWTKey
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [
    AppController,
    SocialRedirectController,
    AuthBaseController,
    AuthVkController,
    AuthGoogleController,
    AuthFbController,
    AuthGithubController,
    UserInfoController,
    RefreshController,
    CheckController,
  ],
  providers: [
    AuthLogger,
    UserService,
    AppService,
    AuthService,
    JwtStrategy,
    DBConfigService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(UserIdMiddleware)
      .forRoutes('user-info', 'auth/vk', 'auth/fb', 'auth/google', 'auth/github');
  }

  onModuleInit(): void {
    // pass
  }

}
