import { Injectable, LoggerService, Logger } from '@nestjs/common';


function AsBrowserConsole(target, key, descriptor): PropertyDescriptor {
    if(descriptor === undefined) {
      descriptor = Object.getOwnPropertyDescriptor(target, key);
    }
    var originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
        let message = '', context = null;
        switch (args.length) {
            case 0: 
                break;
            case 1:
                message = args[0];
                break;
            case 2:
                [context, message] = args;
                break;
            default:
                let messageArgs;
                [context, ...messageArgs] = args;
                message = messageArgs.join(', ');
        }
        return originalMethod.apply(this, [message, context]);
    };
    return descriptor;
}


/**
 * Для использования логгера во ВСЁМ коде Nest (даже не в моём) нужно в main.ts:
 *      NestFactory.create(AppModule, {logger: false});
 *      app.useLogger(app.get(AuthLogger))
 * Но не стоит так делать, так как тогда все не мои логи будут развёрнуты
 */
export class AuthLogger extends Logger {

    @AsBrowserConsole
    info(...data) {
        const [message, context] = data;
        return super.log(message, context);
    }

    @AsBrowserConsole
    warn(...data) {
        const [message, context] = data;
        return super.warn(message, context);
    }

    @AsBrowserConsole
    debug(...data) {
        const [message, context] = data;
        return super.debug(message, context);
    }

}
