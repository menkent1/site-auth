import { Test, TestingModule } from '@nestjs/testing';
import { AuthLogger } from './auth-logger.service';

describe('AuthLogger', () => {
  let service: AuthLogger;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthLogger],
    }).compile();

    service = module.get<AuthLogger>(AuthLogger);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
