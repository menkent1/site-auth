import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JWTPayload } from '../../jwt/jwt-payload.interface';
import { JwtService } from '@nestjs/jwt';
import { IUser } from '../../db-schemas/user/user.interface';
import { JWT_ISSUER, JWT_AUDIENCE, JWT_EXPIRE, JWT_IDENTIFY, JWT_REFRESH_EXPIRE } from '../../jwt/jwt.consts';
import { getNowNumberDate } from '../../utils/utils';
import { AuthLogger } from '../auth-logger/auth-logger.service';

@Injectable()
export class AuthService {
    constructor(
        private readonly logger: AuthLogger,
        private readonly userService: UserService,
        private readonly jwtService: JwtService) { }

    expirationToken() {return getNowNumberDate() + JWT_EXPIRE * 1000;}

    private geterateJWTPayload(user: IUser): JWTPayload {
        // exp ставить не нужно, так как JWTSign сама его ставит
        return {
            iss: JWT_ISSUER,
            aud: JWT_AUDIENCE,
            iat: getNowNumberDate(),
            sub: user._id,
            upn: user.name,
            roles: user.roles || [],
            jti: JWT_IDENTIFY,
        }
    }

    async signIn(user: IUser): Promise<string> {
        const payload: JWTPayload = this.geterateJWTPayload(user);
        payload.exp = this.expirationToken();
        return this.jwtService.signAsync(payload);
    }

    async signInRefreshToken(user: IUser): Promise<string> {
        const payload: JWTPayload = this.geterateJWTPayload(user);
        payload.exp = getNowNumberDate() + JWT_REFRESH_EXPIRE * 1000;
        return this.jwtService.signAsync(payload);
    }

    private validatePayload(payload: JWTPayload | any) {
        return !!(payload && payload.sub
            && payload.exp >= getNowNumberDate()
            && payload.iss === JWT_ISSUER 
            && payload.aud === JWT_AUDIENCE
            && payload.jti === JWT_IDENTIFY);
    }

    private getValidPayload(token: string): any {
        const tokenObj = this.jwtService.decode(token);
        return this.validatePayload(tokenObj) ? tokenObj : null;
    }

    async validateUser(token: string | any): Promise<boolean> {
        // TODO: Потом проверять, не пора ли обновлять токен, если пора, то отправить соответствующий ответ на клиент
        // А клиент должен сам позаботиться об обновлении токена
        return this.validatePayload(token);
    }

    async getTokenByRefreshToken(tokenString: string): Promise<string> {
        const token = this.jwtService.decode(tokenString);
        if (!this.validatePayload(token)) {
            return '';
        }
        let user: IUser = await this.userService.findUserById(token.sub);
        if (!user) {
            return '';
        }
        return this.signInRefreshToken(user);
    }

    async getTokenRoles(tokenString: string) : Promise<string[]> {
        const token = this.getValidPayload(tokenString);
        return token && token.roles || [];
    }
}
