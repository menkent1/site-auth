import { ISocialParams } from "../../db-schemas/social-params/social-params.interface";
import { Model } from 'mongoose';
import { Injectable } from "@nestjs/common";


@Injectable()
export class ConfigService {
    private envConfig: { [key: string]: string } = {};

    public publicJWTKey = '';
    public privateJWTKey = '';

    private socialConfig: { [key: string]: string | any; } = [];

    private _socialParamsModel: Model<ISocialParams>;
    public get socialParamsModel(): Model<ISocialParams> {
        return this._socialParamsModel;
    }
    public set socialParamsModel(value: Model<ISocialParams>) {
        this._socialParamsModel = value;

        value.find({}).exec((err, docs) => {
            this.socialConfig = {};
            docs.filter(({ socialName }) => !!socialName).map(e => this.socialConfig[e.socialName] = e);
        });
    }

    constructor() {
        try {
            this.envConfig = require('../../enviroments/common.env').default;
            const envFileName = process.env.NODE_ENV ? process.env.NODE_ENV : 'local';
            const envConfig = require(`../../enviroments/${envFileName}.env`).default;
            Object.assign(this.envConfig, envConfig);
            console.warn('============== LOAD CONFIGURATIONS =====================');
            console.warn(this.envConfig);

            // console.warn('============== LOAD KEYS =====================');
            const fs = require('fs');
            this.privateJWTKey = fs.readFileSync(this.get('JWT_PRIVATE_KEY'));
            this.publicJWTKey = fs.readFileSync(this.get('JWT_PUBLIC_KEY'));
            console.warn('============== LOAD KEYS COMPLETE =====================');
        } catch (e) {
            console.warn('Config load FAIL:', e);
        }
    }

    get(key: string): string {
        return this.envConfig[key];
    }

    getSocialParams(socialName: string, socialProp: string, defaultValue: string = ''): string {
        const socialConf = this.socialConfig[socialName];
        return socialConf && socialConf[socialProp] || defaultValue;
    }

}
