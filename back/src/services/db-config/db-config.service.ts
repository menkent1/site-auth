import { InjectModel } from "@nestjs/mongoose";
import { SOCIAL_PARAMS_SCHEMA } from "../../db-schemas/db-schema.consts";
import { ISocialParams } from "../../db-schemas/social-params/social-params.interface";
import { Model } from 'mongoose';
import { ConfigService } from "../config/config.service";

/**
 * Сервис необходим для связывания ConfigService с socialParamsModel, иначе не получилось добавить модель в него
 * так как коннект к базе и инициализация моделей идут после появления ConfigService в системе
 */
export class DBConfigService {
    constructor(@InjectModel(SOCIAL_PARAMS_SCHEMA) private readonly socialParamsModel: Model<ISocialParams>, private config: ConfigService) {
        this.config.socialParamsModel = socialParamsModel;
    }
}
