import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser, IAuthorizationInfo } from '../../db-schemas/user/user.interface';
import { getNowNumberDate } from '../../utils/utils';
import { USER_SCHEMA } from '../../db-schemas/db-schema.consts';
import { AuthLogger } from '../auth-logger/auth-logger.service';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(USER_SCHEMA) private readonly userModel: Model<IUser>,
        private readonly logger: AuthLogger) {}

    async registerUser(name: string, authInfo: any): Promise<IUser> {
        this.logger.info('UserService::registerUser::', name, authInfo);

        const user: IUser = {
            name: name,
            createDate: getNowNumberDate(),
            lastUpdate: getNowNumberDate(),
            authInfo: authInfo
        } as IUser;

        return this.createUser(user);
    }

    async createUser(user: IUser): Promise<IUser> {
        const createdUser = new this.userModel(user);
        this.logger.warn('createdUser before save:: ', createdUser);
        return await createdUser.save();
    }

    async findUserById(id: string): Promise<IUser> {
        try {
            const user = await this.userModel.findById(id);
            return user;
        } catch (e) {
            return null;
        }
    }

    async findUserByAuth(authInfoMap: {[key: string]: IAuthorizationInfo}): Promise<IUser> {
        const [key, authInfo] = Object.entries(authInfoMap)[0];
        const kk = `authInfo.${key}.id`;
        const query = { [kk]: authInfo.id };
        return this.userModel.findOne(query);
    }

    async updateUser() {
        // this.userModel.updateOne
    }

    async updateAuthInfo(user: IUser | any, authInfoMap: {[key: string]: IAuthorizationInfo}) {
        user.authInfo = new Map([...(user.authInfo as any).entries(), ...Object.entries(authInfoMap)]);
        return user.save();
    }

    async getUserRoles() {}

    async updateUserRoles() {}

    async findAll(): Promise<IUser[]> {
        return await this.userModel.find().exec();
      }
}
