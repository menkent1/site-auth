import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { IUser } from './db-schemas/user/user.interface';
import { AuthLogger } from './services/auth-logger/auth-logger.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private readonly logger: AuthLogger) {}

  @Get()
  testWork(): string {
    return this.appService.testWork();
  }
}
